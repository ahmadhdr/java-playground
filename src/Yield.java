public class Yield {
  public static void main(String[] args) {
    String nilai = "A";
    // yield digunakan untuk mengembalikan nilai didalam switch lambda hanya suppoert di versi java 14 keaaats
    String ucapan = switch(nilai) {
        case "A": yield "nilai anda adalah A";
        case "B", "C" : yield "nilai anda adalah A";
        case "D":
          yield "nilai anda adalah D";
        default:
          yield "nilai anda adalah A";
    }
    System.out.println(ucapan);
  } 
}
