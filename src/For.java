public class For {
  public static void main(String[] args) {
    // infininte loop
    // for(;;) {
    //   System.out.println("GK Berhenti");
    // }
    // tanpa menggunakan init statement
    // byte counter  = 1;
    // for(; counter <= 10;) {
    //   System.out.println("Perulangan ke - " + counter);
    //   counter++;
    // }
    // // menggunakan init statement
    // for(var counter = 1; counter <= 10;) {
    //   System.out.println("Perulangan ke - " + counter);
    //   counter++;
    // }
    // menggunakan post statement
     for(var counter = 1; counter <= 10;counter++) {
      System.out.println("Perulangan ke - " + counter);
    }
  }
}
