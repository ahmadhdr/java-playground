public class Foreach {
  public static void main(String[] args) {
    String[] names = {
      "ahmad","haidar","albaqir"
    };

    // menggunakan for biasa
    // for(byte counter = 0; counter < names.length; counter++) {
    //   System.out.println("Namanya adalah :" + names[counter]);
    // }
    // menggunakan for each
    for(var value: names) {
      System.out.println("Namanya adalah :" + value);
    }
  } 
}
