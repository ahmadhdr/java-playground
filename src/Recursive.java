public class Recursive {
  public static void main(String[] args) {
    var result = factorial(10);
    System.out.println(result);
  }
  static int factorial(int value) {
    if(value == 1) {
      return 1;
    }
    return value * factorial(value - 1);
  }
}
