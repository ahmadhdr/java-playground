public class TipeDataPrimitive {
  public static void main (String[] args) {
    /**
     * Tipe data primitive tidak memiliki default value, dan pada saat pembuatan variable tersebut harus di inisialisasi nilainya.
     */
    Integer iniinteger2;
    Integer iniInteger = 100;
    Long iniLong = 10000L;
    Byte iniByte = 100;
  }
}
