public class OperasiBoolean {
  public static void main(String[] args) {
    byte nilai_ujian = 80;
    byte nilai_uas = 80;
    
    boolean is_ujian_lulus = nilai_ujian >= 80;
    boolean is_uas_lulus = nilai_uas >= 80;

    boolean lulus = is_uas_lulus && is_uas_lulus;
    System.out.println(lulus);
  }
}
