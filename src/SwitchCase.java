public class SwitchCase {
 public static void main(String[] args) {
   //  support di java 14
   byte nilai = 10;
   switch(nilai) {
     case 10:
      System.out.println("nilai anda sepuluh");
      break;
     default: 
      System.out.println("nilai anda tidak terdifinisi");
      break;
   }
 } 
}
