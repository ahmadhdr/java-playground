public class TypeDataArray {
  public static void main(String[] args) {
    /**
     * Tedapat banyak cara membuat sebuah array
     */
    /**
     * CARA PERTAMA
     */
    String[] student_nim;
    student_nim = new String[3];
    student_nim[0] = "1000";
    student_nim[1] = "01TPLK001";
    student_nim[2] = "01TPLK002";
    System.out.println(student_nim[0]);
    /**
     * CARA KEDUA
     */
    int[] number = new int[] {
      10,20,30,40
    };
    System.out.println(number.length);

    /**
     * CARA KETIGA
     */

    String[] name = {
      "ahmad", "haidar","albaqir"
    };
    System.out.println(name);
    

    /**
     * Array multideminsei
     */
    String[][] asdasd= {
      {"ahmad","haidar"}
    };
  }
}
