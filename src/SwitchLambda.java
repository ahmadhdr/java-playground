public class SwitchLambda {
  public static void main(String[] args) {
    String nilai = "A";
   //  support di java 14
    switch(nilai) {
      case "A" -> System.out.println("nilai anda adalah A");
      case "B", "C" -> System.out.println("nilai anda adalah A");
      case "D" -> {
        System.out.println("nilai anda adalah D");
      };
      default -> {
        System.out.println("nilai anda adalah A");
      }
    }
  }
}
