public class MethodParameter {
  public static void main(String[] args) {
    sayHello("AHMAD","HAIDAR");
  }

  static void sayHello(String name, String lastname) {
    System.out.println("Hallo ," + name + " " + lastname);
  }
}
