public class TipeDate {
  public static void main(String[] args) {
    byte iniByte = 123; // maximal 127 minimal -128
    short iniShort = 1000;
    int iniInt = 100000000;
    
    long iniLong = 100000000L;

    float iniFloat = 10.10F;
    double iniDouble = 10.10;

    int decimalInt = 34;
    int hexadecimal = 0xFFFFF;
    int binary = 0b10110010;

    // sejak versi java 8 atau 7 bisa menambahkan underscore  untuk memudahkan untuk membaca nomor nya

    // konversi tipe data number 
    /**
     * Terdapat 2 jenis konversi tipe data number
     * Widening casting ( Otomatis): byte -> short -> int -> long -> float -> double
     * Narrowing casting ( Manual ): double -> float -> long -> int -> short -> byte
     */
  }
}
