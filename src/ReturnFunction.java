public class ReturnFunction {
  public static void main(String[] args) {
   var result = sum(6,10);
   System.out.println(result);
  }

  static int sum(int one, int two) {
    return one + two;
  }
}
